﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;

namespace Kreis_Code {

    /// <summary>
    /// Manages the decoding of a CircleCode according to the specification.
    /// </summary>
    public static partial class Decoder {

        private static List<Symbol> Specification = null;

        // Those are tolerances used for finding concentric circles. A wider range may lead to false detections, but is less sensitive to dirty images.
        private const double Ratio12Min = 1.50, Ratio12Max = 1.85; // Exact is 1.(6)
        private const double Ratio13Min = 2.15, Ratio13Max = 2.50; // Exact is 2.(3)
        // The resolution used for scanning the data ring
        private const int ScanRes = 360;

        //Lookup tables dramatically reduce runtime
        private static readonly double[] SinLookup;
        private static readonly double[] CosLookup;

        /// <summary>
        /// Initializes the lookup tables
        /// </summary>
        static Decoder() {
            SinLookup = new double[RRes];
            CosLookup = new double[RRes];

            for (int i = 0; i < SinLookup.Length; i++) {
                SinLookup[i] = Math.Sin(Math.PI / RRes * 2 * i);
                CosLookup[i] = Math.Cos(Math.PI / RRes * 2 * i);
            }
        }

        /// <summary>
        /// Decodes the CircularCodes into their representative counterparts.
        /// </summary>
        public static List<CircleCode> Decode(bool[,] pixels, bool[,] edges, int width, int height) {
            var circles = CHTAnalysis(edges, width, height);

            return Decode(pixels, circles, width, height);
        }


        /// <summary>
        /// Decodes the CircularCodes into their representative counterparts.
        /// </summary>
        private static List<CircleCode> Decode(bool[,] pixels, List<Circle> circles, int width, int height) {

            var codes = new List<CircleCode>();

            // Find concentric circles
            // A code is validated, when it consists of at least the inner circle and an outer one in the right distance
            var validated = new List<Circle>();
            for (int i = 0; i < circles.Count; i++) {
                for (int j = 0; j < circles.Count; j++) {
                    double ri = circles[i].r, rj = circles[j].r;
                    if (i != j && Math.Abs(circles[i].x - circles[j].x) <= 2 && Math.Abs(circles[i].y - circles[j].y) <= 2 &&
                        ((ri / rj <= Ratio12Max && ri / rj >= Ratio12Min) || (rj / ri <= Ratio12Max && rj / ri >= Ratio12Min) ||
                        (ri / rj <= Ratio13Max && ri / rj >= Ratio13Min) || (rj / ri <= Ratio13Max && rj / ri >= Ratio13Min))) {
                        Circle exi = validated.Find(c => c.x == circles[i].x && c.y == circles[i].y);
                        Circle exj = validated.Find(c => c.x == circles[j].x && c.y == circles[j].y);
                        if (circles[i].r < circles[j].r) {
                            if (exi == Circle.Null) validated.Add(circles[i]);
                            if (exj != Circle.Null) validated.Remove(circles[j]);
                        } else {
                            if (exj == Circle.Null) validated.Add(circles[j]);
                            if (exi != Circle.Null) validated.Remove(circles[i]);
                        }
                    }
                }
            }


            const double distRatio = 3.3; // The distance in which the outer circle is positioned
            var scan = new bool[ScanRes];
            foreach (Circle c in validated) {
                // Detect the pixels of the data ring
                for (int s = 0; s < ScanRes; s++) {
                    int x = c.x - (int) (c.r * distRatio * CosLookup[s]);
                    int y = c.y - (int) (c.r * distRatio * SinLookup[s]);
                    if (x >= 0 && y >= 0 && x < width && y < height)
                        scan[s] = pixels[x, y];
                }

                // Do run length encoding for error correction and easy analysis
                var rle = new List<Tuple<int, bool>>();
                for (int s = 0; s < ScanRes; s++) {
                    // Incrementing (or replacing due to questionable design of the .NET libraries) the last element, because it's the same
                    if (rle.Count > 0 && rle[rle.Count - 1].Item2 == scan[s])
                        rle[rle.Count - 1] = new Tuple<int, bool>(rle[rle.Count - 1].Item1 + 1, rle[rle.Count - 1].Item2);
                    // otherwise appending to end of list
                    else
                        rle.Add(new Tuple<int, bool>(1, scan[s]));
                }
                // Merge beginning and end when the same
                if (rle.Count > 1 && rle[0].Item2 == rle[rle.Count - 1].Item2) {
                    rle[0] = new Tuple<int, bool>(rle[0].Item1 + rle[rle.Count - 1].Item1, rle[0].Item2);
                    rle.RemoveAt(rle.Count - 1);
                }

                // Decoding the rle list into 16bit integer
                ushort binary = 0;
                const int fragmentation = 16;
                const double segWidth = RRes / fragmentation;
                for (int segment = 0; segment < rle.Count; segment++) {
                    // The width of a segment is a multiple of 360/16 degrees, small errors < 11 pixels don't matter
                    for (int w = 0; w < Math.Round(rle[segment].Item1 / segWidth); w++) {
                        binary <<= 1;
                        if (rle[segment].Item2) binary |= 0x1;
                    }
                }
                ushort mask = 0xFFFF;
                foreach (Symbol sym in Specification) {
                    for (int begin = 0; begin <= 16; begin++) {
                        if (binary == ((sym.DBinary >> begin) & mask)) {
                            codes.Add(new CircleCode(c.x, c.y, c.r, sym));
                        }
                    }
                }
            }

            return codes;
        }

        /// <summary>
        /// Load the specification from bin2ascii.txt and put it into the according list.
        /// </summary>
        public static void LoadSpecification() {
            Specification = new List<Symbol>();
            try {
                using (StreamReader sr = new StreamReader("bin2ascii.txt")) {
                    while (!sr.EndOfStream) {
                        String[] split = sr.ReadLine().Split(' ');
                        Specification.Add(new Symbol(Convert.ToUInt16(split[0], 2), Convert.ToByte(split[1]), split[2]));
                    }
                }
            } catch (Exception e) {
                MessageBox.Show("The file could not be read", e.Message);
                return;
            }

        }

        /// <summary>
        /// Represents a single element of the specification. Connects the visual ascii representation to the binary encoding.
        /// </summary>
        public struct Symbol {
            private ushort binary;
            private byte identifier;
            private String data;

            public Symbol(ushort binary, byte index, String data) {
                this.binary = binary;
                this.identifier = index;
                this.data = data;
            }

            internal uint DBinary {
                get { return ((uint) binary << 16) + binary; }
            }

            internal byte Identifier {
                get {
                    return identifier;
                }
            }

            internal object Repr {
                get {
                    if (data == "True") return true;
                    if (data == "False") return false;
                    return Data;
                }
            }

            internal String Data {
                get {
                    if (data == "???") return Convert.ToString(binary, 16);
                    return data;
                }
            }

            public override string ToString() {
                return Data;
            }
        }

    }
}