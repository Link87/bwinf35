﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;

namespace Kreis_Code {

    public static partial class Decoder {

        private const int RMin = 20, RMax = 64 /*300*/, RStep = 17, RRes = 360;

        private static int[,] AccVisualization;

        /// <summary>
        /// Performs basic (remember KISS rule) Circle Hough Transformation (CHT) on the image to find circles.
        /// As the radius is unknown, this function uses a 3D parameter space resulting in
        /// cubic space and time complexity: O(width * height * delta_radius).
        /// This function needs an edge filter to be applied in advance, otherwise it won't work.
        /// </summary>
        /// <param name="pixels">The pixels in which to look for circles.</param>
        /// <returns>A list of <code>Cirlce</code>s that were found.</returns>
        private static List<Circle> CHTAnalysis(bool[,] pixels, int width, int height) {
            var circles = new List<Circle>();
            var accumulator = new ushort[width, height, RStep];

            for (int r0 = RMin; r0 < RMax; r0 += RStep) {

                //Fill the accumulator with values
                Parallel.For(0, height, y0 => {
                    for (int x0 = 0; x0 < width; x0++) {
                        for (int r = r0; r < r0 + RStep; r++) {
                            if (pixels[x0, y0]) {
                                for (int i = 0; i < RRes; i++) {
                                    int x = x0 - (int) (r * CosLookup[i]);
                                    int y = y0 - (int) (r * SinLookup[i]);
                                    if (x >= 0 && x < width && y >= 0 && y < height)
                                        accumulator[x, y, r - r0] += 1;
                                }
                            }
                        }
                    }
                });
                Console.WriteLine("Transformation Done! (from " + r0 + " to " + (r0 + RStep - 1) + ")");

                //Find the highest value and scale the whole accu into an 8-bit greyscale image (_debug)
                AccVisualization = new int[width, height];
                int peak = 0, _maxval = 0;
                Parallel.For(0, height, y => {
                    for (int x = 0; x < width; x++) {
                        for (int r = r0; r < r0 + RStep; r++) {
                            AccVisualization[x, y] += accumulator[x, y, r - r0];
                            if (peak < accumulator[x, y, r - r0])
                                peak = accumulator[x, y, r - r0];
                        }
                        if (_maxval < AccVisualization[x, y]) _maxval = AccVisualization[x, y];
                    }
                });

                ConcurrentBag<Circle> bag = new ConcurrentBag<Circle>();
                Parallel.For(0, height, y => {
                    for (int x = 0; x < width; x++) {
                        for (int r = r0; r < r0 + RStep; r++) {
                            if (accumulator[x, y, r - r0] >= RRes / 50 * r) {
                                bag.Add(new Circle(x, y, r));
                            }
                        }
                        AccVisualization[x, y] = (int) ((float) AccVisualization[x, y] / _maxval * 255);
                    }
                });
                Console.WriteLine("Maxima found!");

                //Merge all circles that fall into the singularity of each other and have approximately the same radii
                foreach (Circle code in bag)
                    circles.Add(code);
                for (int a = 0; a < circles.Count; a++) {
                    for (int b = 0; b < circles.Count;) {
                        int singularity = Math.Min(circles[a].r, circles[b].r) / 3;
                        if (a != b && Math.Abs(circles[a].x - circles[b].x) < singularity &&
                            Math.Abs(circles[a].y - circles[b].y) < singularity && Math.Abs(circles[a].r - circles[b].r) <= singularity)
                            circles.RemoveAt(b);
                        else b++;
                    }
                }
            }
            
            GC.Collect();

            return circles;
        }

       public static int[,] AccVisualizationData() {
            return AccVisualization;
        }

        [SuppressMessage("Microsoft.Design", "CS0660")]
        [SuppressMessage("Microsoft.Design", "CS0661")]
        private struct Circle {
            public readonly int x, y, r;

            public static readonly Circle Null;

            public Circle(int x, int y, int r) {
                this.x = x;
                this.y = y;
                this.r = r;
            }

            // Returns whether both circles are nearly concentric
            public static bool operator == (Circle c1, Circle c2) {
                if (c1 == null || c2 == null) return false;
                return c1.x == c2.x && c1.y == c2.y && c1.r == c2.r;
            }

            public static bool operator !=(Circle c1, Circle c2) {
                if (c1 == null || c2 == null) return false;
                return !(c1.x == c2.x && c1.y == c2.y && c1.r == c2.r);
            }

        }

    }

}
