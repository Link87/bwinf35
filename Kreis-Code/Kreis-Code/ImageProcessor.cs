﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading.Tasks;

namespace Kreis_Code {

    public sealed class CCScanner : IDisposable {

        private readonly Bitmap image;
        private readonly int width, height;
        public List<CircleCode> Codes { get; private set; }

        public delegate void ProgressEventHandler(object sender, ProgressEventArgs e);
        public event ProgressEventHandler Scanned;

        public CCScanner(Bitmap img) {
            image = new Bitmap(img.Width, img.Height, PixelFormat.Format24bppRgb);

            using (Graphics gr = Graphics.FromImage(image)) {
                gr.DrawImage(img, new Rectangle(0, 0, img.Width, img.Height));
            }

            width = img.Width;
            height = img.Height;
        }

        ~CCScanner() {
            image.Dispose();
        }

        /// <summary>
        /// Searches for Circular Codes in the given image.
        /// </summary>
        public async Task ScanAsync() {
            await Task.Factory.StartNew(() => {

                Bitmap gaussImg = FilterGaussian(image);

                var srcPixels = BinaryImage(gaussImg);

                var optPixels = FilterClosing(srcPixels);
                optPixels = FilterOpening(optPixels);

                var edgePixels = MorphBoundaries(optPixels);

                OnProgress(gaussImg, "Entrauscht", Icons.blur_on);
                OnProgress(srcPixels, "Binärisiert", Icons.filter_bw);
                OnProgress(optPixels, "Optimiert", Icons.photo_filter);
                OnProgress(edgePixels, "Kanten", Icons.border_outer);

                Codes = Decoder.Decode(optPixels, edgePixels, width, height);

                OnProgress(Decoder.AccVisualizationData(), "Akkumulator", Icons.accumulator);

            });
        }

        /// <summary>
        /// Converts the image into its binary representation, using the BinaryThreshold.
        /// </summary>
        /// <returns>A boolean 2d-array representing the binary image. True means the specific pixel is black, false means it's white.</returns>
        public unsafe bool[,] BinaryImage(Bitmap img) {
            var pixels = new bool[width, height];

            var bounds = new Rectangle(0, 0, width, height);
            var data = img.LockBits(bounds, ImageLockMode.ReadWrite, image.PixelFormat);

            // unmanaged code neccessary here
            // Fun fact: Parallel Computing reduced needed time for binarization from > 10s to < 0.5s - now imagine the same on OpenCL or CUDA
            byte* ptr0 = (byte*) data.Scan0;
            int stride = data.Stride;

            // Get the brightness of the darkest and brightest pixel. This allows to normalize the brightness below.
            int min = 255 * 3, max = 0;
            Parallel.For(0, height, y => {
                byte* ptr = ptr0 + y * stride;
                for (int x = 0; x < bounds.Width; x++) {
                    int b = *ptr + *(++ptr) + *(++ptr);
                    if (b < min) min = b;
                    if (b > max) max = b;
                    ptr++;
                }
            });

            byte threshold = (byte) ((min + max) / 5);

            // Calculating average rgb value for each pixel
            // When below threshold, then it's made black, otherwise white
            // This results in a relatively clean binary black-white image
            // Threshold fits best to given examples for now, but may be needed to change later on
            Parallel.For(0, height, y => {
                byte* ptr = ptr0 + y * stride;
                for (int x = 0; x < bounds.Width; x++) {
                    int avg = (*ptr + *(++ptr) + *(++ptr)) / 3;
                    pixels[x, y] = avg < threshold;
                    ptr++;
                }
            });

            img.UnlockBits(data);

            return pixels;
        }

        /// <summary>
        /// Applies a Gaussian Filter to the image. The kernel is built from the 2D normal distribution function.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private unsafe Bitmap FilterGaussian(Bitmap source) {
            var bounds = new Rectangle(0, 0, width, height);
            var src = source.LockBits(bounds, ImageLockMode.ReadWrite, image.PixelFormat);
            var srcPixels = new byte[width, height, 3];

            byte* ptr0 = (byte*) src.Scan0;
            int stride = src.Stride;
            
            Parallel.For(0, height, y => {
                byte* ptr = ptr0 + y * stride;
                for (int x = 0; x < bounds.Width; x++) {
                    srcPixels[x, y, 2] = *ptr;
                    srcPixels[x, y, 1] = *(++ptr);
                    srcPixels[x, y, 0] = *(++ptr);
                    ptr++;
                }
            });

            source.UnlockBits(src);

            var destPixels = new byte[width, height, 3];
            Array.Copy(srcPixels, destPixels, width * height * 3);
            
            //Make Gaussian kernel of size 3 with standard deviation 5
            const int kRad = 3;
            const double sigma = 5;
            double sum = 0;
            var kernel = new double[2 * kRad + 1, 2 * kRad + 1];
            for (int x = -kRad; x <= kRad; x++) {
                for (int y = -kRad; y <= kRad; y++) {
                    double val = Math.Exp(- (x * x + y * y) / (2 * sigma * sigma)) / (2 * Math.PI * sigma * sigma);
                    kernel[x + kRad, y + kRad] = val;
                    sum += val;
                }
            }

            // Normalize the kernel
            for (int x = -kRad; x <= kRad; x++)
                for (int y = -kRad; y <= kRad; y++)
                    kernel[x + kRad, y + kRad] /= sum;
                
            // Apply kernel on the image. The result is written into a temporary 3D-Array. 
            Parallel.For(0, height, y => {
                for (int x = 0; x < width; x++) {
                    byte r = 0, g = 0, b = 0;
                    for (int kx = -kRad; kx <= kRad; kx++) {
                        for (int ky = -kRad; ky <= kRad; ky++) {
                            if (x + kx >= 0 && y + ky >= 0 && x + kx < width && y + ky < height) {
                                r += (byte) (srcPixels[x + kx, y + ky, 0] * kernel[kx + kRad, ky + kRad]);
                                g += (byte) (srcPixels[x + kx, y + ky, 1] * kernel[kx + kRad, ky + kRad]);
                                b += (byte) (srcPixels[x + kx, y + ky, 2] * kernel[kx + kRad, ky + kRad]);
                            }
                        }
                    }
                    destPixels[x, y, 0] = r;
                    destPixels[x, y, 1] = g;
                    destPixels[x, y, 2] = b;
                }
            });

            //Transfer the array into a 24bit rgb image
            var dest = new Bitmap(width, height, PixelFormat.Format24bppRgb);
            var data = dest.LockBits(bounds, ImageLockMode.ReadWrite, image.PixelFormat);

            ptr0 = (byte*) data.Scan0;
            stride = data.Stride;

            Parallel.For(0, height, y => {
                byte* ptr = ptr0 + y * stride;
                for (int x = 0; x < bounds.Width; x++) {
                    *ptr = destPixels[x, y, 2];
                    *(++ptr) = destPixels[x, y, 1]; ;
                    *(++ptr) = destPixels[x, y, 0]; ;
                    ptr++;
                }
            });

            dest.UnlockBits(data);

            srcPixels = null;
            destPixels = null;
            GC.Collect();

            return dest;

        }


        /// <summary>
        /// An image filter that removes background noise from the image.
        /// This is done by applying the dilation filter after the erosion filter.
        /// </summary>
        private bool[,] FilterOpening(bool[,] img) {
            var filtered = MorphErosion(img);
            filtered = MorphDilation(filtered);

            return filtered;
        }


        /// <summary>
        /// An image filter that fills small imperfections and holes in the image.
        /// This is done by applying the dilation filter and then the erosion filter.
        /// </summary>
        private bool[,] FilterClosing(bool[,] img) {
            var filtered = MorphDilation(img);
            filtered = MorphErosion(filtered);

            return filtered;
        }


        /// <summary>
        /// Morphologically erodes the image using a 3x3 structuring element.
        /// </summary>
        private bool[,] MorphErosion(bool[,] img) {
            var tmp = new bool[width, height];
            Array.Copy(img, tmp, width * height);
            Parallel.For(0, width, x => {
                for (int y = 0; y < height; y++) {
                    if (!((x > 0 && y > 0 && img[x - 1, y - 1]) && (x > 0 && img[x - 1, y]) && (x > 0 && y < height - 1 && img[x - 1, y + 1]) &&                   // Left side of grid
                    (y > 0 && img[x, y - 1]) && img[x, y] && (y < height - 1 && img[x, y + 1]) &&                                                                  // Vertical center stripe of grid
                    (x < width - 1 && y > 0 && img[x + 1, y - 1]) && (x < width - 1 && img[x + 1, y]) && (x < width - 1 && y < height - 1 && img[x + 1, y + 1])))  // Right side of grid
                        tmp[x, y] = false;
                }
            });

            return tmp;
        }


        /// <summary>
        /// Morphologically dilates the image using a 3x3 grid as structuring element.
        /// </summary>
        private bool[,] MorphDilation(bool[,] img) {
            var tmp = new bool[width, height];
            Array.Copy(img, tmp, width * height);
            Parallel.For(0, width, x => {
                for (int y = 0; y < height; y++) {
                    if ((x > 0 && y > 0 && img[x - 1, y - 1]) || (x > 0 && img[x - 1, y]) || (x > 0 && y < height - 1 && img[x - 1, y + 1]) ||                     // Left side of grid
                    (y > 0 && img[x, y - 1]) || img[x, y] || (y < height - 1 && img[x, y + 1]) ||                                                                  // Vertical center stripe of grid
                    (x < width - 1 && y > 0 && img[x + 1, y - 1]) || (x < width - 1 && img[x + 1, y]) || (x < width - 1 && y < height - 1 && img[x + 1, y + 1]))   // Right side of grid
                        tmp[x, y] = true;
                }
            });
            return tmp;
        }


        /// <summary>
        /// Morphological edge filter (may be replayced by Sobel filter in the future?).
        /// Applies erosion on temporary image tmp. The diff between tmp and the original image is the edges.
        /// </summary>
        private bool[,] MorphBoundaries(bool[,] img) {
            var tmp = new bool[width, height];
            Array.Copy(img, tmp, width * height);
            Parallel.For(0, width, x => {
                for (int y = 0; y < height; y++) {
                    if (!((x > 0 && y > 0 && img[x - 1, y - 1]) && (x > 0 && img[x - 1, y]) && (x > 0 && y < height - 1 && img[x - 1, y + 1]) &&                   // Left side of grid
                    (y > 0 && img[x, y - 1]) && img[x, y] && (y < height - 1 && img[x, y + 1]) &&                                                                  // Vertical center stripe of grid
                    (x < width - 1 && y > 0 && img[x + 1, y - 1]) && (x < width - 1 && img[x + 1, y]) && (x < width - 1 && y < height - 1 && img[x + 1, y + 1])))  // Right side of grid
                        tmp[x, y] = false;
                }
            });
            Parallel.For(0, width, x => {
                for (int y = 0; y < height; y++) {
                    tmp[x, y] = img[x, y] && !tmp[x, y];
                }
            });

            return tmp;
        }

        private unsafe Bitmap Render24bppImage(bool[,] pixels) {
            var img = new Bitmap(width, height, PixelFormat.Format24bppRgb);

            var bounds = new Rectangle(0, 0, width, height);
            var data = img.LockBits(bounds, ImageLockMode.ReadWrite, image.PixelFormat);

            byte* ptr = (byte*) data.Scan0;
            int stride = data.Stride;

            Parallel.For(0, width, x => {
                for (int y = 0; y < height; y++) {
                    if (pixels[x, y]) {
                        ptr[(x * 3) + y * stride] = 0;
                        ptr[(x * 3) + y * stride + 1] = 0;
                        ptr[(x * 3) + y * stride + 2] = 0;
                    } else {
                        ptr[(x * 3) + y * stride] = 255;
                        ptr[(x * 3) + y * stride + 1] = 255;
                        ptr[(x * 3) + y * stride + 2] = 255;
                    }
                }
            });

            img.UnlockBits(data);

            return img;
        }

        private unsafe Bitmap Render24bppImageFromInt(int[,] pixels) {

            Bitmap bm = new Bitmap(width, height, PixelFormat.Format24bppRgb);

            Rectangle bounds = new Rectangle(0, 0, width, height);
            BitmapData data = bm.LockBits(bounds, ImageLockMode.ReadWrite, image.PixelFormat);

            byte* ptr = (byte*)data.Scan0;
            int stride = data.Stride;

            Parallel.For(0, width, x => {
                for (int y = 0; y < height; y++) {
                    ptr[(x * 3) + y * stride] = (byte) pixels[x, y];
                    ptr[(x * 3) + y * stride + 1] = (byte) pixels[x, y];
                    ptr[(x * 3) + y * stride + 2] = (byte) pixels[x, y];
                }
            });

            bm.UnlockBits(data);

            return bm;

        }

        private void OnProgress(bool[,] pixels, String description, Image icon) {
            if (Scanned == null) return;

            Bitmap bm = Render24bppImage(pixels);

            OnProgress(bm, description, icon);

        }

        private void OnProgress(int[,] pixels, String description, Image icon) {
            if (Scanned == null) return;

            Bitmap bm = Render24bppImageFromInt(pixels);

            OnProgress(bm, description, icon);

        }

        private void OnProgress(Bitmap bm, String description, Image icon) {
            if (Scanned == null) return;

            var args = new ProgressEventArgs(bm, description, icon);
            Scanned(this, args);
        }

        /// <summary>
        /// Draws the Codes onto the given Bitmap.
        /// </summary>
        /// <param name="img">Bitmap to draw on</param>
        public void DrawCodeOverlay(Bitmap img) {
            DrawCodeOverlay(img, Codes);
        }

        /// <summary>
        /// Draws the Codes onto the given Bitmap.
        /// </summary>
        /// <param name="img">Bitmap to draw on</param>
        /// <param name="codes">Codes to draw</param>
        private void DrawCodeOverlay(Bitmap img, List<CircleCode> codes) {
            if (codes == null) return;
            using (var gx = Graphics.FromImage(img)) {

                var pen = new Pen(Color.Purple, 3);
                var pen2 = new Pen(Color.Turquoise, 3);

                foreach (CircleCode c in codes) {
                    int s = (int) (c.r * 3.3);

                    gx.DrawLine(pen, c.x - s, c.y, c.x + s, c.y);
                    gx.DrawLine(pen, c.x, c.y - s, c.x, c.y + s);

                    gx.DrawEllipse(pen, c.x - c.r, c.y - c.r, 2 * c.r, 2 * c.r);
                    gx.DrawEllipse(pen2, c.x - s, c.y - s, 2 * s, 2 * s);

                    gx.DrawString(c.sym.ToString(), new Font(FontFamily.GenericMonospace, 42, FontStyle.Bold),
                        new SolidBrush(Color.DarkCyan), c.x, c.y + s);
                }
            }
        }

        public void Dispose() {
            image.Dispose();
        }

    }

    public class ProgressEventArgs : EventArgs {

        public ProgressEventArgs(Bitmap img, String description, Image icon) {
            Image = img;
            Description = description;
            Icon = icon;
        }

        public Bitmap Image { get; private set; }

        public String Description { get; private set; }

        public Image Icon { get; private set; }

    }

}
