﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace Kreis_Code {

    /// <summary>
    /// Manages the UI. Not relevant for algorithmic concerns.
    /// </summary>
    public partial class Frame : Form {

        private String path;

        private CCScanner processor;

        private List<Bitmap> bmps;
        private int activeImg = 0;

        private int ActiveImg {
            get { return activeImg; }
            set {
                if (value >= 0 && value < bmps.Count)
                    activeImg = value;
            }
        }

        public Frame() {
            bmps = new List<Bitmap>();
            InitializeComponent();
        }

        private void ResetFrame() {
            buttonStart.Enabled = true;
            buttonPrev.Enabled = false;
            buttonNext.Enabled = false;
            buttonSave.Enabled = false;
            buttonShow.Enabled = false;
            buttonShow.Checked = false;

            for (int i = buttonNext.DropDown.Items.Count - 1; i > 0; i--)
                buttonNext.DropDown.Items.RemoveAt(i);

            labelInfo.Text = "";

            if (processor != null) {
                processor.Dispose();
                processor = null;
            }

            for (int i = 0; i < bmps.Count; i++)
                bmps[i].Dispose();
            bmps.Clear();
            ActiveImg = 0;

            GC.Collect();
        }

        private void UpdateFrame() {

            buttonPrev.Enabled = true;
            buttonNext.Enabled = true;

            if (picbox.Image != null)
                picbox.Image.Dispose();

            Bitmap img = (Bitmap) bmps[ActiveImg].Clone();
            if (buttonShow.Enabled && buttonShow.Checked) {
                processor.DrawCodeOverlay(img);
                picbox.Image = img;
            }
            else picbox.Image = img;

            if (buttonShow.Enabled)
                labelInfo.Text = "Ansicht: " + buttonNext.DropDown.Items[ActiveImg].Text;

            Invalidate();
        }

        private void UpdateFrame(Bitmap newImage, String description, Image icon) {

            if (newImage != null && description != null && description != "") {
                labelInfo.Text = "Suche Kreis-Codes...";
                bmps.Add(newImage);
                buttonNext.DropDown.Items.Add(description, icon);
                ActiveImg = bmps.Count - 1;
            }

            UpdateFrame();

        }

        private void ButtonOpen_Click(object sender, EventArgs e) {

            OpenFileDialog dialog = new OpenFileDialog();

            if (dialog.ShowDialog() == DialogResult.OK) {
                ResetFrame();

                path = dialog.FileName;
                Text = "Kreis-Codes - " + path;
                Bitmap img = new Bitmap(path);
                bmps.Add(img);
                picbox.Image = (Bitmap) img.Clone();
            }
        }

        private async void ButtonStart_Click(object sender, EventArgs e) {

            if (processor == null) {
                Bitmap img = new Bitmap(path);
                labelInfo.Text = "Filter anwenden...";
                buttonStart.Enabled = false;
                buttonOpen.Enabled = false;
                processor = new CCScanner(img);
                processor.Scanned += PostUpdate;

                Cursor.Current = Cursors.WaitCursor;
                await processor.ScanAsync();

                Cursor.Current = Cursors.Arrow;
                buttonShow.Enabled = true;
                buttonShow.Checked = true;
                buttonOpen.Enabled = true;
                buttonSave.Enabled = true;

                ActiveImg -= 2;
                UpdateFrame();
                GC.Collect();
            }
        }

        private void ButtonPrev_Click(object sender, EventArgs e) {
            ActiveImg--;
            UpdateFrame();
        }

        private void ButtonNext_Click(object sender, EventArgs e) {
            ActiveImg++;
            UpdateFrame();
        }

        private void ButtonShow_Click(object sender, EventArgs e) {
            UpdateFrame();
        }

        private void ButtonSave_Click(object sender, EventArgs e) {
            var dialog = new SaveFileDialog() {
                AddExtension = true,
                DefaultExt = ".png",
                Filter = "Unterstützte Bilddateien (*.png)|*.png"
            };
            if (dialog.ShowDialog() == DialogResult.OK) {
                String path = dialog.FileName;

                if (picbox.Image != null)
                    picbox.Image.Save(path, ImageFormat.Png);
            }
        }

        private void PostUpdate(object sender, ProgressEventArgs e) {
            Invoke(new MethodInvoker(() => UpdateFrame(e.Image, e.Description, e.Icon)));
        }

        private void DropDownItem_Click(object sender, ToolStripItemClickedEventArgs e) {
            Console.WriteLine(e.ClickedItem.Text);
            int i = 0;
            for (; i < buttonNext.DropDown.Items.Count; i++)
                if (buttonNext.DropDown.Items[i].Text == e.ClickedItem.Text) break;
            ActiveImg = i;
            UpdateFrame();
        }
    }
}
