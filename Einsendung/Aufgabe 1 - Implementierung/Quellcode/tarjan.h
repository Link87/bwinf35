#ifndef ROSINEN_TARJAN_H
#define ROSINEN_TARJAN_H

int tarjan(int n, std::vector<node>& graph, std::vector<int>& comps);

void tarjan(int n, std::vector<node>& graph, std::vector<int>& comps, int i);

#endif //ROSINEN_TARJAN_H
