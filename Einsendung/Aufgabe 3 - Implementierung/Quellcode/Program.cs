﻿using System;
using System.Windows.Forms;

namespace Kreis_Code {

    public class Program {

        [STAThread]
        public static void Main() {
            Decoder.LoadSpecification();

            Application.EnableVisualStyles();
            Application.Run(new Frame());

        }

    }

    /// <summary>
    /// Represents a decoded CircleCode
    /// </summary>
    public class CircleCode {
        public readonly int x, y, r;
        public readonly Decoder.Symbol sym;

        public CircleCode(int x, int y, int r, Decoder.Symbol sym) {
            this.x = x;
            this.y = y;
            this.r = r;
            this.sym = sym;
        }

    }

}