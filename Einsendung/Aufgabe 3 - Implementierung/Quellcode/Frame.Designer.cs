﻿namespace Kreis_Code
{
    partial class Frame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel = new System.Windows.Forms.Panel();
            this.picbox = new System.Windows.Forms.PictureBox();
            this.toolstrip = new System.Windows.Forms.ToolStrip();
            this.buttonOpen = new System.Windows.Forms.ToolStripButton();
            this.buttonSave = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.buttonPrev = new System.Windows.Forms.ToolStripButton();
            this.buttonNext = new System.Windows.Forms.ToolStripSplitButton();
            this.originalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonShow = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.buttonStart = new System.Windows.Forms.ToolStripButton();
            this.labelInfo = new System.Windows.Forms.ToolStripLabel();
            this.panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbox)).BeginInit();
            this.toolstrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.Controls.Add(this.picbox);
            this.panel.Controls.Add(this.toolstrip);
            this.panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel.Location = new System.Drawing.Point(0, 0);
            this.panel.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.panel.Name = "panel";
            this.panel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.panel.Size = new System.Drawing.Size(1656, 1063);
            this.panel.TabIndex = 0;
            // 
            // picbox
            // 
            this.picbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.picbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picbox.Location = new System.Drawing.Point(0, 38);
            this.picbox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.picbox.Name = "picbox";
            this.picbox.Size = new System.Drawing.Size(1656, 1025);
            this.picbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picbox.TabIndex = 1;
            this.picbox.TabStop = false;
            // 
            // toolstrip
            // 
            this.toolstrip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.toolstrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolstrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolstrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buttonOpen,
            this.buttonSave,
            this.toolStripSeparator1,
            this.buttonPrev,
            this.buttonNext,
            this.buttonShow,
            this.toolStripSeparator2,
            this.buttonStart,
            this.labelInfo});
            this.toolstrip.Location = new System.Drawing.Point(0, 0);
            this.toolstrip.Name = "toolstrip";
            this.toolstrip.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.toolstrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolstrip.Size = new System.Drawing.Size(1656, 38);
            this.toolstrip.Stretch = true;
            this.toolstrip.TabIndex = 0;
            this.toolstrip.Text = "toolStrip1";
            // 
            // buttonOpen
            // 
            this.buttonOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonOpen.Image = global::Kreis_Code.Icons.open;
            this.buttonOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonOpen.Margin = new System.Windows.Forms.Padding(5, 1, 2, 2);
            this.buttonOpen.Name = "buttonOpen";
            this.buttonOpen.Size = new System.Drawing.Size(28, 35);
            this.buttonOpen.Text = "Open";
            this.buttonOpen.Click += new System.EventHandler(this.ButtonOpen_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonSave.Enabled = false;
            this.buttonSave.Image = global::Kreis_Code.Icons.save;
            this.buttonSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonSave.Margin = new System.Windows.Forms.Padding(2, 1, 2, 2);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(28, 35);
            this.buttonSave.Text = "Save";
            this.buttonSave.Click += new System.EventHandler(this.ButtonSave_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 38);
            // 
            // buttonPrev
            // 
            this.buttonPrev.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonPrev.Enabled = false;
            this.buttonPrev.Image = global::Kreis_Code.Icons.navigate_before;
            this.buttonPrev.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonPrev.Margin = new System.Windows.Forms.Padding(2, 1, 2, 2);
            this.buttonPrev.Name = "buttonPrev";
            this.buttonPrev.Size = new System.Drawing.Size(28, 35);
            this.buttonPrev.Text = "Previous";
            this.buttonPrev.Click += new System.EventHandler(this.ButtonPrev_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonNext.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.originalToolStripMenuItem});
            this.buttonNext.Enabled = false;
            this.buttonNext.Image = global::Kreis_Code.Icons.navigate_next;
            this.buttonNext.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonNext.Margin = new System.Windows.Forms.Padding(2, 1, 2, 2);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(51, 35);
            this.buttonNext.Text = "Next";
            this.buttonNext.ButtonClick += new System.EventHandler(this.ButtonNext_Click);
            this.buttonNext.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.DropDownItem_Click);
            // 
            // originalToolStripMenuItem
            // 
            this.originalToolStripMenuItem.Image = global::Kreis_Code.Icons.photo;
            this.originalToolStripMenuItem.Name = "originalToolStripMenuItem";
            this.originalToolStripMenuItem.Size = new System.Drawing.Size(199, 38);
            this.originalToolStripMenuItem.Text = "Original";
            // 
            // buttonShow
            // 
            this.buttonShow.CheckOnClick = true;
            this.buttonShow.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonShow.Enabled = false;
            this.buttonShow.Image = global::Kreis_Code.Icons.mark;
            this.buttonShow.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonShow.Margin = new System.Windows.Forms.Padding(2, 1, 2, 2);
            this.buttonShow.Name = "buttonShow";
            this.buttonShow.Size = new System.Drawing.Size(28, 35);
            this.buttonShow.Text = "Show Marks";
            this.buttonShow.Click += new System.EventHandler(this.ButtonShow_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 38);
            // 
            // buttonStart
            // 
            this.buttonStart.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonStart.Enabled = false;
            this.buttonStart.Image = global::Kreis_Code.Icons.play_arrow;
            this.buttonStart.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonStart.Margin = new System.Windows.Forms.Padding(2, 1, 2, 2);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(28, 35);
            this.buttonStart.Text = "Start";
            this.buttonStart.Click += new System.EventHandler(this.ButtonStart_Click);
            // 
            // labelInfo
            // 
            this.labelInfo.ForeColor = System.Drawing.Color.Black;
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(0, 35);
            // 
            // Frame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.ClientSize = new System.Drawing.Size(1656, 1063);
            this.Controls.Add(this.panel);
            this.ForeColor = System.Drawing.Color.White;
            this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.MinimumSize = new System.Drawing.Size(574, 511);
            this.Name = "Frame";
            this.ShowIcon = false;
            this.Text = "Kreis-Codes";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbox)).EndInit();
            this.toolstrip.ResumeLayout(false);
            this.toolstrip.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.PictureBox picbox;
        private System.Windows.Forms.ToolStrip toolstrip;
        private System.Windows.Forms.ToolStripButton buttonOpen;
        private System.Windows.Forms.ToolStripButton buttonSave;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton buttonStart;
        private System.Windows.Forms.ToolStripButton buttonPrev;
        private System.Windows.Forms.ToolStripSplitButton buttonNext;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel labelInfo;
        private System.Windows.Forms.ToolStripButton buttonShow;
        private System.Windows.Forms.ToolStripMenuItem originalToolStripMenuItem;
    }
}