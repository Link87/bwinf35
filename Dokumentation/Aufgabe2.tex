\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[ngerman]{babel}
\usepackage{scrpage2}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{listingsutf8}
\usepackage{xcolor}
\usepackage{graphicx}
\pagestyle{scrheadings}

\title{Aufgabe 1 - Rosinen picken}
\author{Marvin Gazibarić}
\date{24.04.2017}

\ihead{Marvin Gazibarić}
\chead{Teilnahme-ID: 6800}
\ohead{Rosinen picken}
\cfoot{\pagemark}

\lstdefinestyle{examplestyle}{
	breaklines=true,
	language={},
	numbers=none,
	basicstyle=\footnotesize,
}

\lstdefinestyle{cppstyle}{
	breaklines=true,
	language=C++,
	basicstyle=\footnotesize\ttfamily,
	numbers=left,
	numberstyle=\tiny,
	keywordstyle=\bfseries\color{blue},
	commentstyle=\color{gray}
}

\def\CC{{C\nolinebreak[4]\hspace{-.05em}\raisebox{.4ex}{\tiny\textbf{++}}}}

\begin{document}
	\lstset{style=cppstyle}
	\maketitle
	\tableofcontents
	\newpage
	\section{Idee}
	Ein Firmenkonglomerat soll aufgelöst werden und wir sind an seinen Bruchstücken interessiert. Wir können Firmen beliebig aus dem Konglomerat auswählen, müssen aber Nebenbedingungen beachten. Speziell heißt das, dass der Kauf eines bestimmten Unternehmens uns möglicherweise auch zum Kauf anderer Unternehmen verpflichtet. 
	\subsection{Vorbetrachtungen}
	Das Konglomerat abstrahieren wir mathematisch als gerichteten Graphen $G(V,E)$. Knoten repräsentieren einzelne Firmen, wobei $v:V\to\mathbb{Z}$ jedem Knoten den Wert des entsprechenden Unternehmens zuordnet. Ziel ist es, eine Teilmenge aus $V$ auszuwählen, sodass die Summe der Werte der gewählten Knoten möglichst groß bzw.\ maximal wird.
	
	Gerichtete Kanten geben die besagten Nebenbedingungen an. Eine Kante $(a, b)\in E$ bedeutet, dass auch Knoten $b$ gewählt werden muss, wenn $a$ ausgewählt wird. Dieses Prinzip gilt rekursiv, d.h.\ dass ggf.\ weitere Knoten, die eingehende Kanten von $b$ aus besitzen, ebenfalls gewählt werden müssten.
	
	Zunächst fällt auf, dass Zyklen bzw.\ starken Zusammenhangskomponenten (SZK) damit eine besondere Rolle zukommt. Wird ein Knoten einer SZK gewählt, so müssen demnach auch alle anderen Knoten der SZK gewählt werden, da sie direkt oder indirekt mit dem gewählten Knoten verbunden sind. Zyklen können daher zu einem einzelnen Knoten zusammengefasst werden, der stellvertretend für alle Unternehmen dieses Zyklus steht.
	
	Der \emph{Algorithmus von Tarjan} findet u.a.\ SZK in gerichteten Graphen. Wir bilden zunächst mit ihm einen sogenannten Komponentengraphen $G_K(V_K,E_K)$. Die Knotenmenge wird durch die gefundenen SZK\footnote{Auch nicht zusammenfassbare Teilgraphen von $G$ bilden SZK, diese bestehen dann folglich aus jeweils nur einzelnen Knoten.} gebildet. Die Kanten werden aus $E$ so übernommen, dass sie nun die SZK verbinden, deren Knoten in $G$ miteinander verbunden waren.
	
	\subsection{Tatsächlicher Gewinn}
	Der Wert $v(a)$ eines Knotens $a$ ist an sich wenig aussagekräftig. Eine Firma mit hohem Wert kann die Katze im Sack sein, wenn man durch Nebenbedingungen zu missgewirtschafteten Unternehmen mit negativem Wert gezwungen wird, die den Kauf möglicherweise zu einem Verlustgeschäft machen. Daher berechnen wir für jeden Knoten in $G_K$ den \emph{tatsächlichen Gewinn} $p:V\to\mathbb{Z}$, den ein Kauf abwerfen würde. Dieser ist die Summe aus dem Wert des Knotens selbst und den Werten aller davon durch Bedingungen abhängigen Knoten.
	
	Um $p(a)$ zu berechnen, führen wir für jeden Knoten $A\in V_K$ einzeln eine Tiefensuche aus. Es ist nämlich leider nicht ohne weiteres möglich, den tatsächlichen Gewinn einfach aus den Gewinnen der unmittelbar abhängigen Knoten zu errechnen, da dann indirekt abhängige Knoten möglicherweise doppelt gezählt werden. Sonst würde nämlich eine einzige Tiefensuche über ganz $G_K$ genügen.
	
	\subsection{Prinzip}
	Die Idee des Algorithmus besteht darin, zunächst denjenigen Knoten $a$ mit dem höchsten $p(a)$ auszuwählen. Also den Knoten, der den größten Gewinn verspricht. Diesen entfernen wir mit allen davon abhängigen Knoten aus $V_K$ und markieren alle entsprechenden Firmen als ausgewählt.
	Anschließend berechnen wir für die verbleibenden Knoten die tatsächlichen Gewinne neu und wählen wieder den Knoten mit dem höchsten Gewinn. Das wird iterativ so lange wiederholt, wie der höchste Gewinn noch positiv ist. Schlussendlich sind die Firmen ausgewählt, die den meisten Gewinn abwerfen.
	
	\subsection{Korrektheit}
	Der Algorithmus terminiert immer. In jeder Iteration wird entweder ein weiterer Knoten ausgewählt, was den noch zu untersuchenden Graphen weiter verkleinert, oder aber es gibt keinen weiteren Knoten mehr, der Gewinne bringt, wodurch das Ergebnis gefunden ist. Somit genügt es zu zeigen, dass die Tiefensuche terminiert. Da kein Knoten doppelt besucht wird, hat die Tiefensuche eine endliche Zahl an Schritten und findet damit immer ein Ende.
	
	Leider ist der Algorithmus nicht optimal. Es kann nämlich vorkommen, dass zwei Knoten mit einem jeweils negativen tatsächlichen Gewinn unlukrativ aussehen und daher nicht ausgewählt werden, aber einen gemeinsamen Nachfolger mit ebenfalls stark negativem tatsächlichen Gewinn haben. Würde man beide Knoten wählen, ließe sich möglicherweise sogar Profit machen, da der wertlose Nachfolger nur einmal gekauft werden müsste. 
	
	Beobachten lässt sich dies auch, wenn solche zwei Knoten einen gemeinsamen Vorgänger mit negativem Wert besitzen. Dieser hat dann nämlich möglicherweise einen positiven tatsächlichen Gewinn und wird ausgewählt, obwohl die beiden Knoten einzeln mehr Profit bringen würden.
	
	In den Beispielen zeigten sich aber nur relativ geringe Abweichungen meist im einstelligen oder unteren zweistelligen Bereich, die aber nichtsdestotrotz in der Realität schmerzhafte Einbußen bedeuten können.
	
	\subsection{Laufzeit}
	Das Verfahren besteht aus zwei Teilen. Für den Algorithmus von Tarjan ergibt sich zunächst eine Laufzeit von $\mathcal{O}(\vert V\vert +\vert E\vert)$, da jeder Knoten von der Tiefensuche einmal besucht wird und für jede Kante der inzidente Knoten überprüft wird. $\vert E\vert$ ist nach oben durch $\binom{n}{2}=\frac{n(n-1)}{2}$ beschränkt. Damit gilt mit $n=\vert V\vert$ im worst-case $\mathcal{O}(n^2)$.
	
	Der zweite Teil besteht aus dem Finden des Maximums und der dazu benötigten Unternehmen. Zum Bestimmen der tatsächlichen Gewinne wird $\vert V_K\vert$ mal eine Tiefensuche initiiert, die im schlechtesten Fall ebenfalls $\vert V_K\vert$ Schritte benötigt. Ebenso führt der Algorithmus maximal $\vert V_K\vert$ Iterationen durch, was insgesamt eine Laufzeit von $\mathcal{O}(\vert V_K\vert^3)$ ergibt. $\vert V_K\vert$ und $\vert E_K\vert$ lassen sich für den worst-case mit $\vert V\vert$ bzw.\ $\vert E\vert$ abschätzen.\footnote{Das passiert gdw.\ der Graph bereits azyklisch war.} Als aufwendigster Teil des gesamten Verfahrens ergibt sich somit insgesamt eine kubische Laufzeit von $\mathcal{O}(n^3)$.
	
	Natürlich wäre es möglich, stattdessen einfach die summierten Werte aller möglichen Teilmengen zu berechnen und diejenige mit der größten Summe als optimales Ergebnis anzusehen. Da wir dabei jedes Element der Potenzmenge $\mathcal{P}(V)$ einmalig betrachten, erhalten wir eine Laufzeit von $\mathcal{O}(\vert\mathcal{P}(V)\vert)$. Mit $\vert\mathcal{P}(V)\vert=2^{\vert X\vert}$ ergibt sich damit eine exponentielle Laufzeit, die bei mehr als 20 Firmen nicht mehr in realistischer Zeit berechenbar ist.
	
	\section{Umsetzung}
	Das Programm, in \CC umgesetzt, liest zunächst die Eingabedatei ein, deren Pfad über die Konsole eingegeben wurde. Die Daten werden in einer Adjazenzliste gespeichert, wobei jeder Knoten als \lstinline|node| vorliegt und in einer \lstinline|unordered_set<int>| die ausgehenden Kanten als Indizes der Zielknoten abgelegt sind.
	
	\subsection{Tarjan-Algorithmus}
	Zunächst reduziert der Algorithmus von Tarjan den Graphen zu einem Komponentengraphen:
	\begin{lstlisting}
for (int i = 0; i < n; i++) {
    if (tjn_ind[i] == -1) tarjan(n, graph, comps, i);
}
	\end{lstlisting}
	\begin{lstlisting}
void tarjan(int n, vector<node>& graph, vector<int>& comps, int i) {
  tjn_ind[i] = tjn_low[i] = tjn_cnt++;
  tjn_stack.push(i);
  tjn_stacked[i] = true;

  for (int j : graph[i].adj) {
    if (tjn_ind[j] == -1) { // j not visited yet
      tarjan(n, graph, comps, j);
      tjn_low[i] = min(tjn_low[i], tjn_low[j]);
    }
    else if (tjn_stacked[j]) {
      tjn_low[i] = min(tjn_low[i], tjn_ind[j]);
    }
  }

  if (tjn_ind[i] == tjn_low[i]) {
    int j;
    do {
      j = tjn_stack.top();
      tjn_stack.pop();
      tjn_stacked[j] = false;
      comps[j] = tjn_ccomp;
    } while (i != j);
    tjn_ccomp++;
  }
}
	\end{lstlisting}
	
	In einer Tiefensuche werden dabei jedem Knoten zwei Werte zugewiesen. Der index, hier \lstinline|tjn_ind|, gibt die discovery time des Knotens an, also als wievielter Knoten er in der Tiefensuche aufgerufen wurde. Der lowlink, hier \lstinline|tjn_low| ist hingegen der niedrigste index eines Knotens, mit dem dieser Knoten verbunden ist. Alle Knoten mit demselben lowlink gehören derselben Komponente an.
	
	Für die eigentliche Reduktion zum Komponentengraph wird die Summe aus den Werten der verschmolzenen Knoten als Gesamtwert genommen. Außerdem speichern wir in einer weiteren \lstinline|unordererd_set<int>|, aus welchen Unternehmen der Knoten besteht, um später die ausgewählten Firmen nachvollziehen zu können.
	
	\subsection{Berechnen der Gewinne}
	Das Berechnen der tatsächlichen Gewinne ist auch im Quelltext trivial. Wir setzen zunächst alle Gewinne auf 0 zurück und rufen dann \lstinline|accumulate| für alle Knoten einmal auf.
	\begin{lstlisting}
for (node n : graph) n.acc_val = 0;
	
for (int i = 0; i < graph.size(); i++) {
  if (graph[i].selected) continue;                // ignore selected nodes
  vector<char> visited(graph.size(), 0);
  graph[i].acc_val = accumulate(graph, i, visited);
}
	\end{lstlisting}
	\begin{lstlisting}
/// recursive dfs funktion that does the actual accumulation
int accumulate(vector<node>& graph, int i, vector<char>& visited) {
  visited[i] = 1;
  int value = graph[i].val;
	
  for (int j : graph[i].adj) {
    if (!visited[j] && !graph[j].selected) value += accumulate(graph, j, visited);
  }
	
  return value;
}
	\end{lstlisting}
	
	\subsection{Auswahl der Unternehmen}
	Schlussendlich wird iterativ das Unternehmen mit dem höchsten Wert gewählt und als ausgewählt markiert. Für dieses wird \lstinline|select_comp| aufgerufen, welche rekursiv alle von diesem abhängigen Unternehmen ebenfalls markiert.
	\begin{lstlisting}
int max_val, max_comp;
do {
  accumulate(graph);
	
  // finds the most valuable node/company and marks it as selected
  max_val = 0;
  max_comp = -1;
  for (int i = 0; i < graph.size(); i++) {
    if (graph[i].acc_val > max_val && !graph[i].selected) {
      max_val = graph[i].acc_val;
      max_comp = i;
    }
  }
  if (max_comp != -1) {
    select_comp(graph, max_comp);
  }
} while (max_comp != -1);
\end{lstlisting}
\begin{lstlisting}
void select_comp(vector<node>& graph, int i) {
  graph[i].selected = 1;
  for (int j : graph[i].adj) select_comp(graph, j);
}
	\end{lstlisting}
	
	Die so markierten Unternehmen werden schließlich ausgegeben. Die Ausgabedatei ist gleichnamig zur Eingabedatei mit einem angehängten \glqq .out\grqq.
	
	\section{Beispiele}
	Im Folgenden stehen die Ausgaben des Programms über die Konsole. Der Dokumentation liegen auch Ausgabedateien im geforderten Format bei. Das Programm gibt die Ausgabedatei auch zusätzlich zur normalen Ausgabe auf der Konsole aus.
	\lstset{style=examplestyle}
	\subsection{Kreis8}
	\begin{lstlisting}
Gekaufte Unternehemen: 0(11) 1(-21) 2(41) 3(10) 4(3) 5(-3) 6(-35) 7(-2)
Profit: 4
	\end{lstlisting}
	\subsection{Quadrat6}
	\begin{lstlisting}
Gekaufte Unternehemen: 1(-7) 3(-5) 4(-6) 5(27) 6(45) 7(10) 8(38) 9(12) 11(25) 12(41) 13(-13) 14(32) 15(12) 16(-47) 18(37) 20(-3) 21(27) 22(23) 24(11) 25(42) 27(-9) 29(2) 30(4) 31(3) 33(34) 34(-5) 35(-2)
Profit: 328
	\end{lstlisting}
	\subsection{Quadrat8}
	\begin{lstlisting}
Gekaufte Unternehemen: 2(45) 4(32) 5(12) 8(27) 9(37) 14(-7) 15(27) 17(38) 18(3) 21(-13) 22(11) 23(8) 25(-47) 26(12) 27(3) 28(25) 30(23) 31(-9) 32(37) 33(-4) 35(41) 37(41) 38(10) 39(-31) 40(38) 41(-45) 43(5) 44(25) 45(-48) 48(-3) 49(-34) 50(12) 52(34) 53(48) 54(4) 57(2) 58(42) 60(27) 61(-7) 62(10)
Profit: 431
	\end{lstlisting}
	\subsection{Quadrat13}
	\begin{lstlisting}
Gekaufte Unternehemen: 7(-34) 8(8) 10(23) 12(12) 13(47) 16(15) 17(-7) 18(-17) 23(7) 24(38) 25(-33) 27(-20) 28(13) 29(36) 35(12) 36(36) 39(12) 41(41) 42(-27) 43(-15) 46(-45) 48(25) 57(12) 59(38) 60(32) 62(4) 68(45) 72(7) 74(39) 75(42) 77(1) 83(40) 84(-39) 86(8) 87(8) 88(30) 89(-35) 90(17) 91(44) 92(-9) 93(43) 94(48) 96(-9) 99(47) 100(21) 103(5) 104(-9) 105(21) 106(38) 109(-27) 110(2) 111(-10) 112(-1) 114(28) 115(-19) 116(1) 118(8) 119(3) 120(3) 121(27) 122(17) 123(40) 124(18) 126(2) 129(3) 130(0) 131(36) 132(7) 134(19) 135(28) 137(21) 138(-9) 141(9) 145(-28) 146(15) 148(25) 149(-14) 152(-9) 153(37) 154(-25) 155(-42) 156(-29) 157(30) 158(34) 161(21) 162(47) 163(-34) 165(-5) 166(25) 168(27)
Profit: 897
	\end{lstlisting}
	\subsection{Zufall7}
	\begin{lstlisting}
Keine Unternehmen gekauft
	\end{lstlisting}
	\subsection{Zufall40}
	\begin{lstlisting}
Gekaufte Unternehemen: 0(-20) 1(34) 2(5) 3(31) 6(35) 7(18) 10(41) 11(20) 13(16) 14(7) 15(5) 16(43) 18(27) 20(43) 22(-14) 23(36) 25(45) 26(11) 27(3) 28(32) 31(38) 32(36) 35(-17) 36(3) 37(36) 38(-15)
Profit: 499
	\end{lstlisting}
	\subsection{Zufall100}
	\begin{lstlisting}
Gekaufte Unternehemen: 0(41) 2(21) 4(31) 10(27) 15(9) 20(36) 25(43) 29(36) 34(11) 41(36) 47(25) 53(30) 57(43) 62(26) 63(10) 66(20) 67(16) 81(37) 96(45) 97(29) 98(41)
Profit: 613
	\end{lstlisting}
	\appendix
	\section{Quelltext}
	\lstset{style=cppstyle}
	\subsection{main.h}
	\begin{lstlisting}
#ifndef ROSINEN_MAIN_H
#define ROSINEN_MAIN_H

int main();

/// accumulates the real profit for each and every node
void accumulate(std::vector<node>& graph);

/// recursive dfs function that does the actual accumulation
int accumulate(std::vector<node>& graph, int i,
               std::vector<char>& visited);

/// marks recursively the node and recursively all child nodes as selected
void select_comp(std::vector<node>& graph, int i);

/// Reads a line from the given input stream,
/// ignoring comment lines (those starting with '#')
void readline(std::istream& in, std::string& s) {
  using namespace std;
  do {
    getline(in, s);
  } while(s.size() > 0 && s[0] == '#');
}

#endif //ROSINEN_MAIN_H
	\end{lstlisting}
	\subsection{company.h}
	\begin{lstlisting}
#ifndef ROSINEN_NODE_H
#define ROSINEN_NODE_H
	
#include <unordered_set>
	
struct node {
  int val, acc_val;
  bool selected = false;
  std::unordered_set<int> adj, companies;
};
	
#endif //ROSINEN_NODE_H
	\end{lstlisting}
	\subsection{tarjan.h}
	\begin{lstlisting}
#ifndef ROSINEN_TARJAN_H
#define ROSINEN_TARJAN_H
	
int tarjan(int n, std::vector<node>& graph, std::vector<int>& comps);
	
void tarjan(int n, std::vector<node>& graph, std::vector<int>& comps,
            int i);
	
#endif //ROSINEN_TARJAN_H
	\end{lstlisting}
	\subsection{tarjan.cpp}
	\begin{lstlisting}
#include <cstring>
#include <stack>
#include <vector>
#include <algorithm>
#include "company.h"
#include "tarjan.h"

using namespace std;

// counter incremented each call of tarjan(...)
int tjn_cnt;
//counter that identifies the current component
int tjn_ccomp;
// the dfs index of each node   
vector<int> tjn_ind;
// the so-called lowlink used by Tarjan's algorithm
vector<int> tjn_low;
// using char cuz vector<bool> is unacceptably slow
vector<char> tjn_stacked;
// a stack containing all parent nodes of the currently visited node
stack<int> tjn_stack;              

/// initiates tarjan's algorithm, @returns the component count
int tarjan(int n, vector<node>& graph, vector<int>& comps) {
  tjn_cnt = tjn_ccomp = 0;
  tjn_ind = vector<int>(n, -1);   // -1 means *not visited*
  tjn_low = vector<int>(n, 0);
  tjn_stacked = vector<char>(n, 0);
  
  // Call tarjan(...) for node i, if that node has not been visited yet
  for (int i = 0; i < n; i++) {
    if (tjn_ind[i] == -1) tarjan(n, graph, comps, i);
  }

  return tjn_ccomp;
}

/// the actual implementation of tarjan's dfs
void tarjan(int n, vector<node>& graph, vector<int>& comps, int i) {
  tjn_ind[i] = tjn_low[i] = tjn_cnt++;
  tjn_stack.push(i);
  tjn_stacked[i] = true;

  for (int j : graph[i].adj) {
    if (tjn_ind[j] == -1) { // j not visited yet
      tarjan(n, graph, comps, j);
      tjn_low[i] = min(tjn_low[i], tjn_low[j]);
    }
    else if (tjn_stacked[j]) {
      tjn_low[i] = min(tjn_low[i], tjn_ind[j]);
    }
  }

  if (tjn_ind[i] == tjn_low[i]) {
    int j;
    do {
      j = tjn_stack.top();
      tjn_stack.pop();
      tjn_stacked[j] = false;
      comps[j] = tjn_ccomp;
    } while (i != j);
    tjn_ccomp++;
  }
}
	\end{lstlisting}
	\subsection{main.cpp}
	\begin{lstlisting}
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <stack>
#include <algorithm>
#include <string>
#include "company.h"
#include "tarjan.h"
#include "main.h"

using namespace std;

int main() {

  // open the input file
  string path;
  getline(cin, path);
  // remove " sign at the front
  if (path.size() > 0 && path.front() == '"')
    path.erase(0, 1);
  // remove it from the back
  if (path.size() > 0 && path.back() == '"')
    path.erase(path.size() - 1, 1);
  ifstream fin(path);

  // read node count
  string n_s;
  readline(fin, n_s);
  unsigned int N = stoul(n_s);

  // read the values of each node
  vector<node> nodes(N);
  for (int i = 0; i < N; i++) {
    string d_s;
    readline(fin, d_s);
    istringstream iss(d_s);
    int a, val;
    iss >> a >> val;
    nodes[a].val = val;
  }

  // read the adjacencies
  while (!fin.eof()) {
    string adj_s;
    readline(fin, adj_s);

    if (adj_s.size() <= 0) continue;

    istringstream iss(adj_s);
    int a, b;
    iss >> a >> b;
    nodes[a].adj.insert(b);
  }

  // make the graph acyclic
  vector<int> comps(N, -1);
  // find strongly connected components, returns number of components
  int ccomp = tarjan(N, nodes, comps);
  // merge the nodes of each component into ine single node
  vector<node> graph((unsigned int) ccomp);
  for (int i = 0; i < N; i++) {
    graph[comps[i]].val += nodes[i].val;
    graph[comps[i]].companies.insert(i);
    // write the index of each adjacent component into the adjacency list
    for (int j : nodes[i].adj) {
      if (comps[i] != comps[j]) graph[comps[i]].adj.insert(comps[j]);
    }
  }

  int max_val, max_comp;
  do {
    accumulate(graph);

    // finds the most valuable node/company and marks it as selected
    max_val = 0;
    max_comp = -1;
    for (int i = 0; i < graph.size(); i++) {
      if (graph[i].acc_val > max_val && !graph[i].selected) {
        max_val = graph[i].acc_val;
        max_comp = i;
      }
    }
    if (max_comp != -1) select_comp(graph, max_comp);
  } while (max_comp != -1);

  int sum = 0;
  vector<int> companies;
  for (node n : graph) {
    if (n.selected) {
      for (int c : n.companies) companies.push_back(c);
    sum += n.val;
    }
  }
  sort(companies.begin(), companies.end());
  if (companies.size() == 0) cout << "Keine Unternehmen gekauft" << endl;
  else {
    cout << "Gekaufte Unternehemen: ";
    for (int c : companies) cout << c << '(' << nodes[c].val << ") ";
    cout << endl << "Profit: " << sum << endl;
  }

  string out_path = path + ".out";
  ofstream fout(out_path);
  cout << "Ausgabe nach " << out_path << ":" << endl;
  fout << "# Anzahl der Knoten in der Teilmenge" << endl
       << companies.size() << endl
       << "# Gesamtwert der Knoten" << endl
       << (float) sum << endl
       << "# Nummern der Knoten" << endl;
  cout << "# Anzahl der Knoten in der Teilmenge" << endl
       << companies.size() << endl
       << "# Gesamtwert der Knoten" << endl
       << (float) sum << endl
       << "# Nummern der Knoten" << endl;

  for (int c : companies) {
    fout << c << endl;
    cout << c << endl;
  }

  cout << 0;

  fin.close();
  fout.flush();
  fout.close();

  return 0;
}

void accumulate(vector<node>& graph) {
  for (node n : graph) n.acc_val = 0;

  // determine the actual value of a node by accumulating
  // the values of all child nodes
  for (int i = 0; i < graph.size(); i++) {
    // ignore selected nodes
    if (graph[i].selected) continue;
    // bool vectors are slow and ram is less expensive than cpu time
    vector<char> visited(graph.size(), 0);
    graph[i].acc_val = accumulate(graph, i, visited);
  }
}

int accumulate(vector<node>& graph, int i, vector<char>& visited) {
  visited[i] = 1;
  int value = graph[i].val;

  for (int j : graph[i].adj) {
    if (!visited[j] && !graph[j].selected)
      value += accumulate(graph, j, visited);
  }

  return value;
}

// works fine without a list of visited nodes, because the graph
// is acyclic. It would improve performance though.
void select_comp(vector<node>& graph, int i) {
  graph[i].selected = 1;
  for (int j : graph[i].adj) select_comp(graph, j);
}
	\end{lstlisting}
	
\end{document}