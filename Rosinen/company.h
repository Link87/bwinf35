#ifndef ROSINEN_NODE_H
#define ROSINEN_NODE_H

#include <unordered_set>

struct node {
    int val, acc_val;
    bool selected = false;
    std::unordered_set<int> adj, companies;
};

#endif //ROSINEN_NODE_H
