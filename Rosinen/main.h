#ifndef ROSINEN_MAIN_H
#define ROSINEN_MAIN_H

int main();

/// accumulates the real profit for each and every node
void accumulate(std::vector<node>& graph);

/// recursive dfs function that does the actual accumulation
int accumulate(std::vector<node>& graph, int i, std::vector<char>& visited);

/// marks recursively the node and recursively all child nodes as selected
void select_comp(std::vector<node>& graph, int i);

/// Reads a line from the given input stream, ignoring comment lines (those starting with '#')
void readline(std::istream& in, std::string& s) {
    using namespace std;
    do {
        getline(in, s);
    } while(s.size() > 0 && s[0] == '#');
}

#endif //ROSINEN_MAIN_H
