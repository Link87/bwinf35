#include <cstring>
#include <stack>
#include <vector>
#include <algorithm>
#include "company.h"
#include "tarjan.h"

using namespace std;

int tjn_cnt, tjn_ccomp;             // counter incremented each call of tarjan(...) / counter that identifies the current component
vector<int> tjn_ind, tjn_low;       // the dfs index of each node / the so-called lowlink used by Tarjan's algorithm
vector<char> tjn_stacked;           // using char cuz vector<bool> is unacceptably slow
stack<int> tjn_stack;               // a stack containing all parent nodes of the currently visited node

/// initiates tarjan's algorithm, @returns the component count
int tarjan(int n, vector<node>& graph, vector<int>& comps) {
    tjn_cnt = tjn_ccomp = 0;
    tjn_ind = vector<int>(n, -1);   // -1 means *not visited*
    tjn_low = vector<int>(n, 0);
    tjn_stacked = vector<char>(n, 0);
    //comps.resize(n, -1);

    // Call tarjan(...) for node i, if that node has not been visited yet
    for (int i = 0; i < n; i++) {
        if (tjn_ind[i] == -1) tarjan(n, graph, comps, i);
    }

    return tjn_ccomp;
}

/// the actual implementation of tarjan's dfs
void tarjan(int n, vector<node>& graph, vector<int>& comps, int i) {
    tjn_ind[i] = tjn_low[i] = tjn_cnt++;
    tjn_stack.push(i);
    tjn_stacked[i] = true;

    for (int j : graph[i].adj) {
        if (tjn_ind[j] == -1) { // j not visited yet
            tarjan(n, graph, comps, j);
            tjn_low[i] = min(tjn_low[i], tjn_low[j]);
        }
        else if (tjn_stacked[j]) {
            tjn_low[i] = min(tjn_low[i], tjn_ind[j]);
        }
    }

    if (tjn_ind[i] == tjn_low[i]) {
        int j;
        do {
            j = tjn_stack.top();
            tjn_stack.pop();
            tjn_stacked[j] = false;
            comps[j] = tjn_ccomp;
        } while (i != j);
        tjn_ccomp++;
    }
}
