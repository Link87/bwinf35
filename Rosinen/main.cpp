#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <stack>
#include <algorithm>
#include <string>
#include "company.h"
#include "tarjan.h"
#include "main.h"

using namespace std;

int main() {

    // open the input file
    string path;
	getline(cin, path);
    if (path.size() > 0 && path.front() == '"') path.erase(0, 1);                   // remove " sign at the front
    if (path.size() > 0 && path.back() == '"') path.erase(path.size() - 1, 1);      // remove it from the back
    ifstream fin(path);

    // read node count
    string n_s;
    readline(fin, n_s);
    unsigned int N = stoul(n_s);

    // read the values of each node
    vector<node> nodes(N);
    for (int i = 0; i < N; i++) {
        string d_s;
        readline(fin, d_s);
        istringstream iss(d_s);
        int a, val;
        iss >> a >> val;
        nodes[a].val = val;
    }

    // read the adjacencies
    while (!fin.eof()) {
        string adj_s;
        readline(fin, adj_s);

        if (adj_s.size() <= 0) continue;

        istringstream iss(adj_s);
        int a, b;
        iss >> a >> b;
        nodes[a].adj.insert(b);
    }

    // make the graph acyclic
    vector<int> comps(N, -1);
    int ccomp = tarjan(N, nodes, comps);        // find strongly connected components, returns number of components
    // merge the nodes of each component into ine single node
    vector<node> graph((unsigned int) ccomp);
    for (int i = 0; i < N; i++) {
        graph[comps[i]].val += nodes[i].val;
        graph[comps[i]].companies.insert(i);
        // write the index of each adjacent component into the adjacency list
        for (int j : nodes[i].adj) {
            if (comps[i] != comps[j]) graph[comps[i]].adj.insert(comps[j]);
        }
    }

    int max_val, max_comp;
    do {
        accumulate(graph);

        // finds the most valuable node/company and marks it as selected
        max_val = 0;
        max_comp = -1;
        for (int i = 0; i < graph.size(); i++) {
            if (graph[i].acc_val > max_val && !graph[i].selected) {
                max_val = graph[i].acc_val;
                max_comp = i;
            }
        }
        if (max_comp != -1) select_comp(graph, max_comp);
    } while (max_comp != -1);

    int sum = 0;
    vector<int> companies;
    for (node n : graph) {
        if (n.selected) {
            for (int c : n.companies) companies.push_back(c);
            sum += n.val;
        }
    }
    sort(companies.begin(), companies.end());
    if (companies.size() == 0) cout << "Keine Unternehmen gekauft" << endl;
    else {
        cout << "Gekaufte Unternehemen: ";
        for (int c : companies) cout << c << '(' << nodes[c].val << ") ";
        cout << endl << "Profit: " << sum << endl;
    }

    string out_path = path + ".out";
    ofstream fout(out_path);
    cout << "Ausgabe nach " << out_path << ":" << endl;
    fout << "# Anzahl der Knoten in der Teilmenge" << endl
         << companies.size() << endl
         << "# Gesamtwert der Knoten" << endl
         << (float) sum << endl
         << "# Nummern der Knoten" << endl;
    cout << "# Anzahl der Knoten in der Teilmenge" << endl
         << companies.size() << endl
         << "# Gesamtwert der Knoten" << endl
         << (float) sum << endl
         << "# Nummern der Knoten" << endl;


    for (int c : companies) {
        fout << c << endl;
        cout << c << endl;
    }

    fin.close();
    fout.flush();
    fout.close();

    string a;
    getline(cin, a);

    return 0;
}

void accumulate(vector<node>& graph) {
    for (node n : graph) n.acc_val = 0;

    // determine the actual value of a node by accumulating the values of all child nodes
    for (int i = 0; i < graph.size(); i++) {
        if (graph[i].selected) continue;                // ignore selected nodes
        vector<char> visited(graph.size(), 0);          // bool vectors are slow and ram is less expensive than cpu time
        graph[i].acc_val = accumulate(graph, i, visited);
    }
}

int accumulate(vector<node>& graph, int i, vector<char>& visited) {
    visited[i] = 1;
    int value = graph[i].val;

    for (int j : graph[i].adj) {
        if (!visited[j] && !graph[j].selected) value += accumulate(graph, j, visited);
    }

    return value;
}

// works fine without a list of visited nodes, because the graph is acyclic. It would improve performance though.
void select_comp(vector<node>& graph, int i) {
    graph[i].selected = 1;
    for (int j : graph[i].adj) select_comp(graph, j);
}
